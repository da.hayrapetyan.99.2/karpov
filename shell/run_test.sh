#!/bin/bash

docker pull 89131904761/karpov
docker run --publish 5000:5000 --name service -d --rm waryak/karpov_courses:secret_ci_service
sleep 2

var=$(curl http://65.109.236.1:5000/get_secret_number/test)

docker stop service

docker service create -d --replicas 1  --name web_app_6_1_test --publish 5001:5000 \
 -e PORT=5001 -e SECRET_NUMBER=$var -e REPLICA_NAME="replica" -e HOST=123.143.12 --env-file ./env.list 89131904761/karpov